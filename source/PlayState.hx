package;

import flixel.addons.display.FlxBackdrop;
import flixel.addons.ui.FlxUIAssets;
import flixel.addons.ui.FlxUICheckBox;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxState;
import flixel.FlxSprite;
import openfl.filters.BitmapFilter;
import openfl.filters.BlurFilter;
import openfl.filters.ColorMatrixFilter;

#if shaders_supported
#if (openfl >= "8.0.0")
import openfl8.*;
#else
import openfl3.*;
#end
import openfl.filters.ShaderFilter;
import openfl.Lib;
#end

class PlayState extends FlxState
{
	var filters:Array<BitmapFilter> = [];
	var uiCamera:flixel.FlxCamera;
	var filterMap:Map<String, {filter:BitmapFilter, ?onUpdate:Void->Void}>;
	
	override public function create():Void
	{
		filterMap = [
			#if shaders_supported
			"ExampleShader" => {
				var shader = new ExampleShader();
				{
					filter: new ShaderFilter(shader),
					onUpdate: function() {
						#if (openfl >= "8.0.0")
						shader.uTime.value = [Lib.getTimer() / 1000];
						#else
						shader.uTime = Lib.getTimer() / 1000;
						#end
					}
				}
			},
			#end
		];
		
		var sampleImage = new FlxSprite();
		sampleImage.loadGraphic("assets/images/exampleImage.png");
		add(sampleImage);

		FlxG.game.setFilters(filters);
		
		FlxG.game.filtersEnabled = true;
		filters.push(filterMap.get("ExampleShader").filter);
	}

	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		
		for (filter in filterMap)
		{
			if (filter.onUpdate != null)
				filter.onUpdate();
		}
	}
}