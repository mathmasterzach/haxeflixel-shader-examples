package openfl8;

import flixel.system.FlxAssets.FlxShader;

class ExampleShader extends FlxShader
{
	@:glFragmentSource('
		#pragma header

        uniform float uTime;

		vec3 getCol(vec2 pos){
			vec2 shift=vec2(int(3.*sin(3.*uTime+30.*openfl_TextureCoordv.t)),0.) / openfl_TextureSize.xy;
			return texture2D(bitmap, openfl_TextureCoordv.st + pos + shift).rgb;
		}

		void main()
		{
			vec3 p=vec3(-1.0 / openfl_TextureSize.xy,0.0);

			vec3 col=getCol(p.zz);
			vec3 col2=getCol(p.xz);
			vec3 col3=getCol(p.zy);
			if(length(col)>.1&&((length(col2)>.1&&length(col-col2)>.1)||(length(col3)>.1&&length(col-col3)>.1))){
				col=vec3(0.);
			}

			gl_FragColor = vec4(col,1.0);
		}'
	)
	
	public function new() 
	{
		super();
	}
}